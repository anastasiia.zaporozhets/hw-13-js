const images = Array.from(document.getElementsByClassName("image-to-show"));
let imagesIndex = 0;
let timerId;

function showNextImage (){
    images[imagesIndex].style.display = "none";
    imagesIndex = (imagesIndex + 1) % images.length;
    images[imagesIndex].style.display = "block";
}

function startImagesCycle (){
    images[imagesIndex].style.display = "block";
    timerId = setInterval(showNextImage,3000);
    document.getElementById("start-button").disabled = true;
    document.getElementById("stop-button").disabled = false;
}
startImagesCycle();

function stopImagesCycle () {
    clearInterval(timerId);
    document.getElementById("start-button").disabled = false;
    document.getElementById("stop-button").disabled = true;
}

document.getElementById("start-button").addEventListener("click", startImagesCycle);
document.getElementById("stop-button").addEventListener("click", stopImagesCycle);

























